# Plugins LinkCheck

Ce plugin permet de chercher et tester l’ensemble des liens présents dans les objets.

[Documentation](https://contrib.spip.net/LinkCheck-verificateur-de-liens)

## Todo

* [ ] ajout d’un lien vers archive.org sur les articles morts
* [ ] export CSV (à finaliser avec generer_url_objet)
* [ ] ajout d’un picto lien mort optionnel dans propre() [+ éventuellement lien sur archive.org le cas échéant]
* [ ] vérifier/traiter automagiquement les migrations http⟹https
* [ ] trouver une meilleure regexp pour la détection de lien, ça doit bien exister sous forme de lib
* [ ] ajouter la possibilité de remplacer automatiquement (via un bouton) les liens déplacés par la redirection découverte
